/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rattanalak
 */
public class ConnectDatabaseProject {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has beeen establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }finally{
            if(conn!= null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                     System.out.println(ex.getMessage());
                }
            }
        }
    }
}
