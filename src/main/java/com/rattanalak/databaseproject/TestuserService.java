/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.databaseproject;

import com.rattanalak.databaseproject.model.User;
import com.rattanalak.databaseproject.service.UserService;

/**
 *
 * @author Rattanalak
 */
public class TestuserService {

    public static void main(String[] args) {

        UserService userService = new UserService();
        User user = userService.login("user2", "password");
        if (user != null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("Error");
        }
    }

}
